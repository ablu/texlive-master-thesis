FROM fedora
RUN echo "fastestmirror=true" >> /etc/dnf/dnf.conf
RUN dnf install -y \
    make \
    inkscape \
    latexmk texlive-scheme-minimal \
    texlive-collection-fontsrecommended \
    ImageMagick \
    texlive-cite \
    texlive-beamer \
    texlive-extsizes \
    texlive-smartdiagram texlive-rotating \
    texlive-ascii-font \
    texlive-placeins \
    texlive-acronym \
    texlive-babel-english \
    texlive-cleveref \
    texlive-epstopdf \
    texlive-was \
    pandoc \
    librsvg2-tools \
    dia

RUN curl -L "https://github.com/gliderlabs/sigil/releases/download/v0.4.0/sigil_0.4.0_$(uname -sm|tr \  _).tgz" \
    | tar -zxC /usr/local/bin
